var builder = WebApplication.CreateBuilder(args);

var app = builder.Build();

app.MapGet("/async", async () =>
{
    await Task.Delay(100);
    Console.WriteLine("async");
    return 1;
});

app.MapGet("/sync", () =>
{
    Thread.Sleep(100);
    Console.WriteLine("sync");
    return 1;
});

app.Run();

